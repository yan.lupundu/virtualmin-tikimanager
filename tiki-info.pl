#!/usr/bin/perl
use strict;
use warnings;

use Cwd;
use File::Basename;
use JSON;

our %in;
our $module_name;

$ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
$ENV{'WEBMIN_VAR'} ||= "/var/webmin";

my $pwd = getcwd;
if ($0 =~ /^(.*)\/[^\/]+$/) {
    chdir($pwd = $1);
    $module_name = basename($pwd);
}
require '../virtualmin-tikimanager/virtualmin-tikimanager-lib.pl';

my $d;
my @OLDARGV = @ARGV;

sub usage
{
    print STDERR "$_[0]\n\n" if ($_[0]);
    print "Install Tiki in a virtualmin domain.\n";
    print "\n";
    print "virtualmin tiki-install --domain name\n";
    exit(1);
}

sub exit_error
{
    print STDERR "$_[0]\n";
    exit(1);
}

while(@ARGV > 0) {
    my $arg = shift(@ARGV);
    if ($arg eq "--domain") {
        $in{'domain'} = shift(@ARGV);
    }
    elsif ($arg eq "--help") {
        usage;
    }
}

$in{'domain'} || exit_error("--domain can't be empty");
$d = &virtual_server::get_domain_by("dom", $in{'domain'});
$d || exit_error("The domain '$in{'domain'}' does not exist");

my ($info, $error) = &tikimanager_tiki_info($d);
if ($error) {
    exit_error($error);
}
print to_json($info, {utf8 => 1, pretty => 1});
